package ajaymehta.patternconceptcalcfornextcolumn;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/14/2017.
 */


/*

        1
        2 3
        3 4 5
        4 5 6 7
        5 6 7 8 9
*/
// its same sa Program1 ..but see fist column ..prininting the value of  i...then see second column .. i is increase by 1 ...
    // so you have to do is to print the value of  i... and then add 1 in i  ..if j loop is left (means if it has to print other columns) then it will print the value with added 1..

public class Program2 {


    static Scanner sc = new Scanner(System.in);

    static int adder = 0;


    public static void forloop(int rows) {

        for(int i=1 ; i<= rows; i++) {

            int num= i;

            for(int j=1; j<=i; j++) {

                System.out.print(num+" ");  //fist printing the i value of currect column ..then doing calculation for the next column..
                // calculation for next collumnsss...
                  num = num+1;  // appending in the value if i,,

            } // end of j loop
            System.out.println();
        }


        sc.close();


    }


    public static void main(String args[]) {

        System.out.print("How many rows to create a pattern:");

        int x = sc.nextInt();

        System.out.println();

        forloop(x);
    }

}
