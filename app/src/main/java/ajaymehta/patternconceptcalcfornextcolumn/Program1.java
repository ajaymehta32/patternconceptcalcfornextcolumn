package ajaymehta.patternconceptcalcfornextcolumn;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

/*

        1
        2 2
        3 3 3
        4 4 4 4
        5 5 5 5 5
*/
// it just a simple program printing value of i... ..see Program2 for the concept ..just adding a line will change the whole scenerio..

public class Program1 {

    static Scanner sc = new Scanner(System.in);

    static int adder = 0;


    public static void forloop(int rows) {

      for(int i=1 ; i<= rows; i++) {

          int num= i;

          for(int j=1; j<=i; j++) {

             System.out.print(num+" ");
          } // end of j loop
          System.out.println();
      }


        sc.close();


    }


    public static void main(String args[]) {

        System.out.print("How many rows to create a pattern:");

        int x = sc.nextInt();

        System.out.println();

        forloop(x);
    }


}
